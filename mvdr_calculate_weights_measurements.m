%% MVDR, calculate optimal weights using maximum eigenvalue eigenvector
clear all; clc;
% ----------------------------------------------------------------------------------------
% This script is part of Golam Sadeghnia's master's thesis "Development and
% Evaluation of an External, Steerable Microphone Array for Improving
% Spatial Selectivity of Hearing Aids".
% Golam Sadeghnia 2017, Hearing Systems (DTU), Eriksholm Research Center.

%% Load microphone impulse responses for a given configuration
IR_path = 'Measured Impulse Responses\Set 1'; % chose set 1 or set 2 there
load(fullfile(IR_path, 'measuredRawIRs_frame_entire_sphere_truncated_scaled.mat'))
mic_elements = 1:12;
speaker_elements = 21:44;   % corresponds to AVIL central ring
az_range = 0:15:360-15;     % horizontal DOA
fs = 48e3;                  % sampling frequency of impulse responses

% Prepare cell arrays (structure them into microphone IRs)
n = size(rawIRs_truncated{1}(:,1),1); % length of impulse response in samples
mic_IR = cell(1, size(mic_elements,2));
for m = 1:numel(mic_IR)
    mic_IR{m} = zeros(n, size(az_range,2));
end % for

% For each azimuth angle, assign the apropriate microphone impulse response
for k = 1:numel(speaker_elements)
    for m = 1:numel(mic_elements)
        mic_IR{m}(:,k) = rawIRs_truncated{speaker_elements(k)}(:,mic_elements(m)); % Assign microphone impulse response
    end % for
end % for

%% Calculate the frequency response of each impulse response for each microphone

% Frequency range
L = 2048;
f_range = linspace(0, 0.5*fs, L/2+1)';

% Prepare cell arrays for frequency responses
mic_HRTF = cell(1, size(mic_elements,2));

% Compute frequency response for each microphone
for m = 1:size(mic_elements,2)
    % HRTF (FFT)
    mic_HRTF{m} = fft(mic_IR{m}, L) / L;
    % Use first half, multiply by two to keep energy
    mic_HRTF{m} = 2 * mic_HRTF{m}(1:L/2+1,:);
end % for

%% Calculate optimal MVDR weights for a given angle

% Calculate weights for these look-direction angles
target_az_range = 0:15:360-15;

% Prepare matrix for holding Directiviy Index measures over third-octave bands
subbands = [160 200 250 315 400 500 630 800 1000 1250 1600 2000 2500 3150 4000 5000 6300 8000 12000];
DI_thirdoct = zeros(size(target_az_range,2), size(subbands,2)+1);

% Go through each target angle, compute optimal MVDR weights
wOpt_az = cell(1,numel(target_az_range));
for td = 1:numel(target_az_range) % beamformer look-direction

    % Set target azimuth angle
    target_az = target_az_range(td);

    % Correction factor for incomplete spherical impulse responses
    [~, az_idx] = min(abs(az_range - target_az));
    target_az_n = az_range(az_idx); % nearest target azimuth
    sqrtalpha = repmat(sqrt(abs(sind(az_range-target_az_n)/(length(az_range)-1)))',1, size(mic_elements,2));

    % Prepare matrix for holding weights
    wOpt = zeros(size(mic_HRTF,2), size(f_range,1)); % for all microphones

    % Set logical array for look-direction
    az_idx = az_range == target_az;

    % Calculate Directivity Index (DI) for all DOA for current look-direction

    % Prepare vector for holding directivity index values per frequency
    DI = zeros(size(f_range));

    % Compute weights for each frequency bin
    for f = 1:size(f_range,1)

        % Build Target vector for all microphones
        A = zeros(1, size(mic_elements,2));
        for m = 1:size(mic_elements,2)
            A(m) = mic_HRTF{m}(f, az_idx);
        end % for

        % Build Noise covariance matrix for all microphones
        B = zeros(size(az_range,2),size(mic_elements,2));
        for m = 1:size(mic_elements,2)
            B(:,m) = mic_HRTF{m}(f, :)';
        end % for
        B = conj(B) .* sqrtalpha; % apply correction factor

        % Optimal weights (maximum eigenvalue eigenvector)
        [v, lambda] = eig(A'*A,B'*B);    % lambda: eigenvalues, v: eigenvectors
        [value, index] = max(diag(lambda));
        wOpt(:, f) = v(:, index);
        
        % Directivity index per frequency bin
        DI(f) = 10*log10(2 * abs(A*wOpt(:,f))^2 / abs(pi*(B*wOpt(:,f))'*(B*wOpt(:,f))));

    end % for
    
    % Save optimal MVDR weights for current look-direction
    wOpt_az{td} = wOpt;

    % Directivity Index per third-cctave band
    for ob = 1:numel(subbands)
        oct_idx = f_range>subbands(ob)/sqrt(2) & f_range<subbands(ob)*sqrt(2);
        DI_thirdoct(td,ob) = mean(DI(oct_idx));
    end % for
    DI_thirdoct(td,ob+1) = mean(DI); % over all frequencies

    % Show progress
    fprintf('%d, Directivity Index (average):\t%1.1f dB\n', az_range(td), mean(DI_thirdoct(td,end)));

end % for

% Save MVDR weights in impulse response sub-folder
save(fullfile(IR_path, 'mvdr_weights_entire_horizontal.mat'), 'wOpt_az', 'fs');

