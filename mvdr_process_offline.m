%% Process recorded audio online through beamformer
clear all; clc;
% ----------------------------------------------------------------------------------------
% This script is part of Golam Sadeghnia's master's thesis "Development and
% Evaluation of an External, Steerable Microphone Array for Improving
% Spatial Selectivity of Hearing Aids".
% Golam Sadeghnia 2017, Hearing Systems (DTU), Eriksholm Research Center.

%% Set up device reader/writer and source

% Audio sources
sourcepath = 'Audio Clips\Anechoic';
filename1 = 'DT2_Danish_DHB_Nat_NoPauses.wav';
filename2 = 'DT2_Danish_LBH_Nat_NoPauses.wav';
filename3 = 'HINT_Danish_Nat_LAB_NoPauses.wav';

% Load files
[x1, fsfile] = audioread(fullfile(sourcepath, filename1));
[x2, fsfile] = audioread(fullfile(sourcepath, filename2));
[x3, fsfile] = audioread(fullfile(sourcepath, filename3));

% Only use the first 10 seconds (or minimum duration across the three sources)
mns = min([fsfile*10, numel(x1), numel(x2), numel(x3)]);
x1 = x1(1:mns);
x2 = x2(1:mns);
x3 = x3(1:mns);

% Scale sources
x1 = x1./max(x1);
x2 = x2./max(x2);
x3 = x3./max(x3);

% Resample audio clips to match impulse response sampling rate
fs = 48e3;
x1 = resample(x1,fs,fsfile);
x2 = resample(x2,fs,fsfile);
x3 = resample(x3,fs,fsfile);

%% Configure source DOA and look-direction

% Source DOAs
S = [330, 0, 30];

% Look-direction
target_az = 0;

% Frame length of each block
frameLength = 2048;

% Impulse Responses
IR_path = 'Measured Impulse Responses\Set 1';
load(fullfile(IR_path, 'measuredRawIRs_frame_entire_sphere_truncated_scaled.mat'))

% MVDR weights
resultpath_weights = 'Measured Impulse Responses\Set 1';

% Parameters
mic_elements = 1:12;
speaker_elements = 21:44; % horizontal ring only
az_range = 0:15:360-15;% Azimuth range, that will determine the noise field as well

% Prepare cell arrays
n = size(rawIRs_truncated{1}(:,1),1); % length of impulse response in samples
mic_IR = cell(1, size(mic_elements,2));
for m = 1:numel(mic_IR)
    mic_IR{m} = zeros(n, size(az_range,2));
end % for

% For each azimuth angle, assign the apropriate microphone impulse response
for k = 1:numel(speaker_elements)
    for m = 1:numel(mic_elements)
        mic_IR{m}(:,k) = rawIRs_truncated{speaker_elements(k)}(:,mic_elements(m)); % Assign microphone impulse response
    end % for
end % for

% Load optimal MVDR weights
load(fullfile(resultpath_weights, 'mvdr_weights_entire_horizontal.mat'), 'wOpt_az');

%% Run MVDR Beamformer (using overlap and add)

% Beamformer look direction
td = find(az_range == target_az);   % logical array for look-direction

% Pre-allocate signal vectors for the three sources for all microphones
X = zeros(size(x1,1), numel(mic_elements));

% Place sources by filtering with IRs for source DOA for each microphone
for m = 1:numel(mic_elements)
    X(:,m) = mean([conv(x1, mic_IR{m}(:,S(1)==az_range), 'same'),...    % source 1
        conv(x2, mic_IR{m}(:,S(2)==az_range), 'same'),...               % source 2
        conv(x3, mic_IR{m}(:,S(3)==az_range), 'same')], 2);             % source 3
end % for

% Structure complex MVDR weights
W = [wOpt_az{td}(:, 1:end-1) conj(fliplr(wOpt_az{td}(:, 2:end)))].';
h = ifft(W, frameLength); % time-domain filter coefficients

% Process
Y = fftfilt(h, X);

% Sum and average output
y = mean(Y,2);

% Scale output
y = y/max(abs(y));

%% Binaural Processing
load('CIPIC_hrtf_database\special_kemar_hrir\kemar_horizontal\large_pinna_final.mat')

% Resample KEMAR data to beamformer sampling rate
hrirL = resample(left(:, target_az == 0:5:355), fs, 44.1e3);
hrirR = resample(right(:, target_az == 0:5:355), fs, 44.1e3);

% Process HRIRs in time-domain
yL = conv(y, hrirL, 'same');
yR = conv(y, hrirR, 'same');
yB = [yL yR]; % assemble
yB = yB./max(max(abs(yB))); % scale channel outputs

%% Save assembled anechoic speech
x0 = mean([x1 x2 x3],2);

% Assembled sources (no beamforming)
audiowrite(fullfile(sourcepath, 'assembled_sources.wav'), x0, fs);

% Beamformer monaural output
audiowrite(fullfile('Audio Clips\Processed', sprintf('target_%ddeg_monaural.wav',target_az)), y, fs);
% Beamformer monaural output
audiowrite(fullfile('Audio Clips\Processed', sprintf('target_%ddeg_binaural.wav',target_az)), yB, fs);

%% Evaluate STOI
d = stoi(x0, y, fs);
sprintf('STOI:\t%1.2f', d)
