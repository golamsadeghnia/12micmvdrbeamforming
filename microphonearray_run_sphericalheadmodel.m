%% Simulate impulse responses from entire horizontal plane using head model.
clear all; clc;
% ----------------------------------------------------------------------------------------
% This script is part of Golam Sadeghnia's master's thesis "Development and
% Evaluation of an External, Steerable Microphone Array for Improving
% Spatial Selectivity of Hearing Aids".
% Golam Sadeghnia 2017, Hearing Systems (DTU), Eriksholm Research Center.

%% Configure simulation

% Define microphone locations for the beamformer x, y and z position in
% Euclidian coordinates with respect to centre of origin in meter. Use
% spherical head model radius as reference for microphone array.

% Spherical head model parameters
N_filt = 1000; % filter length
N_order = 40; % filter order
fs = 48e3; % sampling frequency for impulse responses
addpath('Array-Response-Simulator'); % this is the external library

% Head physiognomy
head_r = 0.0875;    % meter, standard measure for spherical head

%% Run simulations
% Run simulations for 15 deg increments DOA around horizontal plane for a
% desired number of microphones and configuration.
% The simulated impulse responses will be stored in h_michead and saved in

% Get array configuration
Nb = 0;         % number of broad-side microphones
Ne = 6;         % number of bilateral end-fire microphones (total = 2x Ne)
Nt = Nb + 2*Ne; % total number of microphones
[mic_dirs_rad, ma_px, ma_py, ma_pz] = microphonearray_configure(head_r, Nb, Ne);

% Define folder for storing impulse response. Create the folder, if it
% doesn't exist
resultpath = sprintf('Simulated Impulse Responses\\%d array elements (%d - %d)', Nt, Nb, Ne);
if ~exist(resultpath, 'dir')
    mkdir(resultpath)
end % if, else

% Show current configuration
clc;
disp([ma_px, ma_py, ma_pz])

% Run spherical head model with 15 deg DOA increments for the horizontal plane
% Define sources (at infinite distance -> plane waves are assumed)
% For each source, compute the HRTF from the head model, and calculate
% microphone outputs.
for src_az = 0:15:360

    fprintf('%d - %d array elements\n', src_az, Nb, Ne);

    % Azimuth and elevation in angles
    src_ang = [src_az, 0];

    % Convert to radians
    src_dirs_rad = [src_ang(1,1) * pi/180, src_ang(1,2) * pi/180];

    % Compute impulse responses based on spherical scatterer
    [h_michead, H_michead] = sphericalScatterer(mic_dirs_rad, src_dirs_rad, head_r, N_order, N_filt, fs);

    % Save array input to beamformer, and beamformer output
    save(fullfile(resultpath, sprintf('head_model_IR_%d_src.mat',src_az)), 'h_michead', 'fs');

end % for
