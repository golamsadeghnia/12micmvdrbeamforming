function [mic_dirs_rad, ma_px, ma_py, ma_pz] = microphonearray_configure(head_r, Nb, Ne)
% Return a pre-determined configuration for a microphone array, based on
% desired number of microphones and head dimensions.
% ----------------------------------------------------------------------------------------
% This script is part of Golam Sadeghnia's master's thesis "Development and
% Evaluation of an External, Steerable Microphone Array for Improving
% Spatial Selectivity of Hearing Aids".
% Golam Sadeghnia 2017, Hearing Systems (DTU), Eriksholm Research Center


% Use typical head ear to ear radius, if none given
if nargin < 1
    head_r = 0.0875; % meter, typical head radius
end % if

% Location of microphone array centre relative to head ratio
ma_x = 0;                   % on axis
ma_y = head_r + 0.015;      % add the thickness of the frame + 0.5 cm distance to head
ma_z = 0.02;                % height of microphone array

% Glasses width
g_x = 0.18; % meter, frame width
g_y = 0.10; % meter, frame length

% Offset from corners
ma_x_offset = 0.01; % m
ma_y_offset = 0.01; % m

% Position of microphones in Cartersian coordinates
% Broad-side (front)
ma_px_b = ma_x + linspace(-g_x/2 + ma_x_offset, g_x/2 - ma_x_offset, Nb)'; % meter, position of microphones on x axis
ma_py_b = ma_y + zeros(size(ma_px_b)); % meter, position of microphones on y axis
ma_pz_b = ma_z + zeros(size(ma_px_b)); % meter, position of microphones on z axis

% Bilateral End-fire (sides)
% Left side
ma_py_el = linspace(ma_y - ma_y_offset, ma_y - g_y + ma_y_offset, Ne)'; %linspace(ma_py_b(1) - ma_y_offset, ma_py_b(1) - ma_y_offset - d_ef*(Ne-1), Ne)';
ma_px_el(1:Ne, 1) = ma_x - g_x/2;
ma_pz_el(1:Ne, 1) = ma_z;
% Right side
ma_py_er = linspace(ma_y - ma_y_offset, ma_y - g_y + ma_y_offset, Ne)';
ma_px_er(1:Ne, 1) = ma_x + g_x/2;
ma_pz_er(1:Ne, 1) = ma_z;

% Combine left/right endfire with broadside
ma_px = [ma_px_el; ma_px_b; ma_px_er];
ma_py = [ma_py_el; ma_py_b; ma_py_er];
ma_pz = [ma_pz_el; ma_pz_b; ma_pz_er];

% Microphone positions in spherical coordinates (azimuth, elevation, radius)
ma_r = sqrt(ma_px.^2 + ma_py.^2 + ma_pz.^2);    % radius
ma_azimuth = atan2d(ma_py, ma_px) - 90;         % offset so that 0 deg is straight ahead
ma_elevation = 90 - acosd(ma_pz ./ ma_r);       % elevation = 90deg - inclination

% Convert to radians
mic_dirs_rad = [ma_azimuth * pi/180, ma_elevation * pi/180, ma_r];
