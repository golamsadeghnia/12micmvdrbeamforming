# 12micMVDRbeamforming

This script is part of Golam Sadeghnia's master's thesis "Development and Evaluation of an External, Steerable Microphone Array for Improving Spatial Selectivity of Hearing Aids"
Golam Sadeghnia 2017-2018, Hearing Systems (DTU), Eriksholm Research Center.