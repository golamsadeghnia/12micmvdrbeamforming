%% Evaluate beamformer Directivity Index for a given target look direction while moving the source around on the horizontal plane.
clear all; clc;
% ----------------------------------------------------------------------------------------
% This script is part of Golam Sadeghnia's master's thesis "Development and
% Evaluation of an External, Steerable Microphone Array for Improving
% Spatial Selectivity of Hearing Aids".
% Golam Sadeghnia 2017, Hearing Systems (DTU), Eriksholm Research Center.

%% Set look-direction of MVDR beamformer
target_az = 0;

%% Load microphone impulse responses for a given configuration
IR_path = 'Simulated Reverberation\Classroom'; % chose between 'Classroom' and 'Auditorium' here
load(fullfile(IR_path, 'revIRs.mat'))
mic_elements = 1:12;
speaker_elements = 21:44; % horizontal ring only
az_range = 0:15:360-15;% Azimuth range, that will determine the noise field as well
fs = 48e3;
resultpath_weights = 'Measured Impulse Responses\Set 1'; % from Set 1
%resultpath_weights = IR_path; % optimal 

% Truncate and rename variable name
rawIRs_truncated = cell(size(revIRs));
for s = speaker_elements
    rawIRs_truncated{s} = revIRs{s}(1:12e3,:);
end % for

% Prepare cell arrays
n = size(rawIRs_truncated{21}(:,1),1); % length of impulse response in samples
mic_IR = cell(1, size(mic_elements,2));
for m = 1:numel(mic_IR)
    mic_IR{m} = zeros(n, size(az_range,2));
end % for

% For each azimuth angle, assign the apropriate microphone impulse response
for k = 1:numel(speaker_elements) %1:numel(az_range)
    for m = 1:numel(mic_elements)
        mic_IR{m}(:,k) = rawIRs_truncated{speaker_elements(k)}(:,mic_elements(m)); % Assign microphone impulse response
    end % for
end % for

%% Calculate the frequency response of each impulse response for each microphone

% Frequency range
n = 2048;
f_range = linspace(0, 0.5*fs, floor(n/2)+1)';

% Prepare cell arrays for frequency responses
mic_HRTF = cell(1, size(mic_elements,2));

% Compute frequency response for each microphone
for m = 1:size(mic_elements,2)
    % HRTF (FFT)
    mic_HRTF{m} = fft(mic_IR{m}, n)/n;
    % Use first half, multiply by two to keep energy
    mic_HRTF{m} = 2 * mic_HRTF{m}(1:floor(n/2+1),:);
end % for

%% Load optimal MVDR weights
load(fullfile(resultpath_weights, 'mvdr_weights_entire_horizontal.mat'), 'wOpt_az');

% Set all weights to unity (disable MVDR beamformer)
% for m = 1:size(wOpt_az,2)
%     wOpt_az{m} = ones(size(wOpt_az{m}));
% end % for

% Set look direction (for optimal MVDR weights)
td = find(az_range == target_az);

% Subbands
subbands = [160 200 250 315 400 500 630 800 1000 1250 1600 2000 2500 3150 4000 5000 6300 8000 12000];
aidi_weights = [0.0083 0.0095 0.0150 0.0289 0.0440 0.0578 0.0653 0.0711 0.0818 0.0844 0.0882 0.0898 0.0868 0.0844 0.0771 0.0527 0.0364 0.0185 0];

% matrix for holding directivity index per subband per evaluated horizotal angle
DI_subband = zeros(size(az_range,2), size(subbands,2)+1);

% AIDI array
AIDI = zeros(size(az_range,2), 1);

% Move source around in the horizontal plane
for sd = 1:numel(az_range)

    % Set current angle
    source_az = az_range(sd);

    % Correction factor for incomplete spherical impulse responses
    [~, az_idx] = min(abs(az_range - source_az));
    source_az_n = az_range(az_idx); % nearest polar azimuth
    sqrtalpha = repmat(sqrt(abs(sind(az_range-source_az_n)/(length(az_range)-1)))',1, size(mic_elements,2));

    % Set logical array for look-direction
    az_idx = az_range == source_az;

    % Prepare vector for holding directivity index values per frequency
    DI = zeros(size(f_range));

    % Compute weights for each frequency step
    for f = 1:size(f_range,1)

        % Build Target vector for all microphones
        A = zeros(1, size(mic_elements,2));
        for m = 1:size(mic_elements,2)
            A(m) = mic_HRTF{m}(f, az_idx);
        end % for

        % Build Noise covariance matrix for all microphones
        B = zeros(size(az_range,2),size(mic_elements,2));
        for m = 1:size(mic_elements,2)
            B(:,m) = mic_HRTF{m}(f, :)';
        end % for
        B = conj(B) .* sqrtalpha; % apply correction factor

        % Directivity index per frequency bin
        DI(f) = 10*log10(2*abs(A*wOpt_az{td}(:,f))^2 / abs(pi*(B*wOpt_az{td}(:,f))' * (B*wOpt_az{td}(:,f))));

    end % for

    % Directivity Index per subband
    for ob = 1:numel(subbands)
        thirdoct_idx = f_range>subbands(ob)/sqrt(2) & f_range<subbands(ob)*sqrt(2);
        DI_subband(sd,ob) = mean(DI(thirdoct_idx));
    end % for
    DI_subband(isnan(DI_subband)) = 0;
    DI_subband(sd,ob+1) = nanmean(DI); % over all frequencies
    AIDI(sd) = sum(DI_subband(sd,1:end-1) .* aidi_weights);

    % Show progress
    fprintf('%d deg, source = %d deg, Directivity Index (average):\t%1.1f dB\n', target_az, az_range(sd), mean(DI_subband(sd,end)));

end % for

% Add the last az = 360 deg from az = 0 deg
DI_subband(end+1,:) = DI_subband(1,:);
AIDI(end+1,:) = AIDI(1,:);

% Normalize (offset) against target direction (average DI)
ref_1kHzdB = DI_subband(td,9);
ref_1kHzdB = 16.0332; % 0 degrees: 16.1866; % from simulation
DI_subband = DI_subband - ref_1kHzdB; %DI_subband(td,end);
AIDI = AIDI - AIDI(td,end);

% Save the DI per subband for the current configuration
save(fullfile(resultpath_weights, sprintf('DI_subband_%d.mat',target_az)), 'DI_subband', 'subbands', 'az_range', 'fs');

% AI-DI at current target
sprintf('%1.1f dB', AIDI(az_range == target_az) - mean(AIDI(az_range ~= target_az)))

%% Plot polar pattern of DI for a certain look-direction
figure(1); clf;
polarplot(deg2rad([az_range 360]), DI_subband(:,1:end-1), 'linewidth', 3.0);
hold on; %title(sprintf('Directivity Index MVDR Per Band\n%d microphones (%d-%d)', size(mic_elements,2), Nb, Ne),'FontSize',16);
rlim([-40, 5]); %legend([string(subbands), 'Entire Spectrum'])
set(gca,'FontSize',16);

figure(2); clf;
polarplot(deg2rad([az_range 360]), AIDI, 'linewidth', 3.0);
hold on; %title(sprintf('AI-DI MVDR\n%d microphones', size(mic_elements,2)),'FontSize',16);
rlim([-40, 5]);
set(gca,'FontSize',16);

%% AIDI for competing talker scenario
% Target = 0 deg
disp(sprintf('%1.1f dB\t%1.1f dB\t%1.1f dB\t%1.1f dB\t%1.1f dB\t%1.1f dB',...
    AIDI(az_range == 330),AIDI(az_range == 0),AIDI(az_range == 30),...
    AIDI(az_range == 90), AIDI(az_range == 180)))

%% Frequency range AI-DI
figure(3); clf; hold on; grid on; box on;
plot(subbands, DI_subband(1,1:end-1) .* aidi_weights)
set(gca,'xscale','log');
axis([120 14e3 -.5 .5]);

%% Frequency range DI
idx0 = az_range == 30;
idxtL = az_range == 0;
idxtR = az_range == 330;
idxb = az_range >= 120 & az_range <= 240;
idxsdL = az_range >= 60 & az_range <= 120;
idxsdR = az_range >= 240 & az_range <= 300;

figure(3); clf; hold on; grid on; box on;
plot(subbands, DI_subband(idx0,1:end-1), 'x-r', 'linewidth', 2.0, 'markersize', 20);% - mean(DI_subband(~idx,1:end-1)));
plot(subbands, mean(DI_subband(idxtL | idxtR,1:end-1)), '.-g', 'linewidth', 2.0, 'markersize', 20);% - mean(DI_subband(~idx,1:end-1)));
plot(subbands, mean(DI_subband(idxsdL,1:end-1)), '.-m', 'linewidth', 2.0, 'markersize', 20);% - mean(DI_subband(~idx,1:end-1)));
plot(subbands, mean(DI_subband(idxb,1:end-1)), '.-k', 'linewidth', 2.0, 'markersize', 20);% - mean(DI_subband(~idx,1:end-1)));
axis([120 13e3 -50 10]); set(gca,'XScale','log');
set(gca,'XTick',subbands(1:19),'fontsize',11);
xlabel('Frequency, Hz','fontsize',16); ylabel('Magnitude, dB','fontsize',18);
h=legend('Target talker 0�','Competing talkers 30� and 330�','Background noise 60�-120�','Background noise 120�-240�','location','bestoutside');
set(h,'fontsize',16);
