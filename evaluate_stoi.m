% STOI evaluation
% ----------------------------------------------------------------------------------------
% This script is part of Golam Sadeghnia's master's thesis "Development and
% Evaluation of an External, Steerable Microphone Array for Improving
% Spatial Selectivity of Hearing Aids".
% Golam Sadeghnia 2017, Hearing Systems (DTU), Eriksholm Research Center.

% Load clean speech
cleanfile = 'Audio Clips\Clean\assembled_sources.wav';
[x, fs] = audioread(cleanfile);

% Load processed speech
processedfile = 'Audio Clips\Processed\target_0_degrees.wav';
[y, fs] = audioread(processedfile);

% Crop to shortest duration
n = min([numel(x) numel(y)]);
x(n+1:end) = [];
y(n+1:end) = [];

% Evaluate STOI
d = stoi(x, y, fs);
disp(d)
